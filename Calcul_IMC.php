<?php

class Personne{

    //Creation des variables
    public float $taille;
    public float $poids;


    // Initialisation des variables
    public function __construct(float $taille, float $poids){

        $this->taille = $taille;
        $this->poids = $poids;
    }

    //Prend la variable Taille
    public function getTaille():float
    {
        return $this->taille;
    }

    //Prend la variable Poids
    public function getPoids():float
    {
        return $this->poids;
    }

    // Definit une valeur a Taille
    public function setTaille(float $taille):void 
    {
         $this->taille = $taille;
    }

    // Definit une valeur a poids
    public function setPoids(float $poids):void 
    {
     $this->poids = $poids;
    }

    public function CalculIMC(){

        $imc = $this->poids / ($this->taille * $this->taille);

        if ($imc < 18.5){
        echo "Maigre, ";
        }
        elseif ($imc > 18.5 && $imc < 25){
        echo "Normal, ";
        }
        else {
        echo "Obese, ";
        }
        return $imc;
}
}

$newimc = new personne ('1.75', '65');
echo "votre imc est de : ". $newimc->CalculIMC();


?>

